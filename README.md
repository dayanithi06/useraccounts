# UserAccounts
#New Update:
1. Added single time login
2. Showing existing user in login page.
3. Last login user data is visible
-------------------------------
#Last Update:
1. Change user login to login and signup.
2. On signup user is created on accounts manager of android device.
3. During login user is checked in accounts manager and login is allowed.
4. Users are shown from accounts manager in top right corner icon for selecting accounts.
5. Entered data is shown for unique users.
-------------------------
1. Splash Screen with 5 seconds of delay
2. Login Screen . Please enter any unique name in username field. password is just for a sake.
3. After Login you can see the accounts detail activity in which user details will appear for particular username.(intially it will be empty)
4. Click on add detail button at bottom to add details. Multiple times detail can be addedfor single user.
5. You can add/switch accounts within app. click on the round icon with intial letter of username on left top corner.(again you can add user details for switched accounts).

Thank You.


