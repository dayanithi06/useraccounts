package com.example.kt_telematicinterview.data;

import android.app.Application;

import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.database.model.DetailModel;
import com.example.kt_telematicinterview.database.model.UserDataModel;

import java.util.ArrayList;

import javax.inject.Inject;

public class DataManagerImpl implements DataManger {
@Inject
DatabaseRealm databaseRealm;
    private final MyApplication application;

    public DataManagerImpl(MyApplication application){
        this.application =application;
        application.getAppComponent().inject(this);
    }

    @Override
    public void addUsers(String username) {
        databaseRealm.addUsers(username);
    }

    @Override
    public ArrayList<DetailModel> retriveDetailsBasedOnUserName(String s) {
        return databaseRealm.retriveUserInformationBasedOnUsername(s);
    }

    @Override
    public ArrayList<UserDataModel> retriveAllDetails() {
        return databaseRealm.retriveAllUsers();
    }

    @Override
    public void addDetails(DetailModel detailModel) {
        databaseRealm.addUserInformation(detailModel);
    }
}
