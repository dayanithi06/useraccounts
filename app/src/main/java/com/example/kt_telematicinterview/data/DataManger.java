package com.example.kt_telematicinterview.data;

import com.example.kt_telematicinterview.database.model.DetailModel;
import com.example.kt_telematicinterview.database.model.UserDataModel;

import java.util.ArrayList;

public interface DataManger {
    void addUsers(String username);

    ArrayList<DetailModel>  retriveDetailsBasedOnUserName(String s);

    ArrayList<UserDataModel> retriveAllDetails();

    void addDetails(DetailModel detailModel);
}
