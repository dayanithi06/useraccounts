package com.example.kt_telematicinterview.data;

import android.app.Application;
import android.content.Context;

import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.database.model.DetailModel;
import com.example.kt_telematicinterview.database.model.UserDataModel;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;

public class DatabaseRealm {

    private Application application = null;
    private Context mContext = null;
    Realm realm = null;

    public DatabaseRealm(MyApplication application) {
        this.application = application;
        application.getAppComponent().inject(this);
        mContext = application.getApplicationContext();
    }

    /**
     * Instance will be thrown
     */
    public Realm getRealmInstance() {
        if (realm != null) {
            return realm;
        }
        //realm = Realm.getInstance(realmConfiguration);
        realm = Realm.getDefaultInstance();
        return realm;
    }

    /**
     * Intial setup will be intialted
     */
    public void setup() {

        Realm.init(mContext);
        RealmConfiguration config =
                new RealmConfiguration.Builder().name("default.realm").deleteRealmIfMigrationNeeded().allowQueriesOnUiThread(true).allowWritesOnUiThread(true)
                        .build();
        Realm.getInstance(config);
        Realm.setDefaultConfiguration(config);

    }

    public void addUsers(String username) {
        final UserDataModel model = new UserDataModel();
        model.setId(1);
        model.setUsername(username);
        Realm realm = getRealmInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(model);
            }
        });
    }

    public UserDataModel retriveUsers(String username){
        UserDataModel model = Realm.getDefaultInstance().where(UserDataModel.class).contains("username",username).findFirst();
        return model;
    }

    public ArrayList<UserDataModel> retriveAllUsers(){
        ArrayList<UserDataModel> list = new ArrayList<>();
        RealmResults<UserDataModel> model = Realm.getDefaultInstance().where(UserDataModel.class).findAll();
       for (UserDataModel model1:model){
           list.add(model1);
       }
        return list;
    }

    public void addUserInformation(final DetailModel detailModel){

        Realm realm = getRealmInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(detailModel);
            }
        });
    }

    public ArrayList<DetailModel> retriveUserInformationBasedOnUsername(String Username){
        ArrayList<DetailModel> list = new ArrayList<>();
        RealmResults<DetailModel> model = Realm.getDefaultInstance().where(DetailModel.class).contains("username",Username).findAll();
        for (DetailModel model1:model){
            list.add(model1);
        }
        return list;
    }
}

