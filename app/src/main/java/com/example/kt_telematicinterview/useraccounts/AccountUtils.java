package com.example.kt_telematicinterview.useraccounts;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.example.kt_telematicinterview.database.model.UserDataModel;

import java.util.ArrayList;

public class AccountUtils {

	public static final String ACCOUNT_TYPE = "com.tele.example";
	public static final String AUTH_TOKEN_TYPE = "com.tele.example.aaa";
	
	//public static IServerAuthenticator mServerAuthenticator = new MyServerAuthenticator();
	
	public static Account getAccount(Context context, String accountName) {
		AccountManager accountManager = AccountManager.get(context);
		Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
		for (Account account : accounts) {
			if (account.name.equalsIgnoreCase(accountName)) {
				return account;
			}
		}
		return null;
	}

	public static  Account checkPasswordIsCorrect(Context context,String accountName,String password){
		AccountManager accountManager = AccountManager.get(context);
		Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
		for (Account account : accounts) {
			if (account.name.equalsIgnoreCase(accountName)) {
				if(password.equals(accountManager.getPassword(account)))
				return account;
			}
		}
		return null;
	}
	public static ArrayList<UserDataModel> getAllAccounts(Context context) {
		ArrayList<UserDataModel> dataModels = new ArrayList<>();
		AccountManager accountManager = AccountManager.get(context);
		Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
		for (Account account : accounts) {
			UserDataModel dataModel = new UserDataModel();
			dataModel.setUsername(account.name);
			dataModels.add(dataModel);
		}
		return dataModels;
	}
}