package com.example.kt_telematicinterview.useraccounts;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.kt_telematicinterview.MainActivity;
import com.example.kt_telematicinterview.R;
import com.example.kt_telematicinterview.database.model.UserDataModel;
import com.example.kt_telematicinterview.ui.LoginActivity;
import com.example.kt_telematicinterview.ui.dialog.LoginUserAccountsDialog;
import com.example.kt_telematicinterview.ui.dialog.SwitchAccountDialog;
import com.example.kt_telematicinterview.viewmodel.LoginViewModel;
import com.example.kt_telematicinterview.viewmodel.MainViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class TAccountActivity extends AppCompatActivity {

    private AccountManager accountManager;

    private final int REQ_REGISTER = 11;
    String userId;
    MainViewModel loginViewModel;
    LoginUserAccountsDialog dialog;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor myEdit;
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_login1);
         sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE);


         myEdit = sharedPreferences.edit();
        loginViewModel = new MainViewModel();
        accountManager = AccountManager.get(getBaseContext());
        if(getIntent().getBooleanExtra("addednewUser",false)){
            //will create new user.
        }else{
            String username = sharedPreferences.getString("UserName", "");
            if (!username.isEmpty()) {
                Intent intent1 = new Intent(TAccountActivity.this, MainActivity.class);
                intent1.putExtra("UserName", username);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
            } else {
                if(!AccountUtils.getAllAccounts(this).isEmpty()){
                    dialog = new LoginUserAccountsDialog();
                    dialog.setViewModel(loginViewModel);
                    Bundle bundle = new Bundle();
                    bundle.putString("username", username);
                    dialog.setArguments(bundle);
                    dialog.show(getSupportFragmentManager(), "acc");
                }

            }
        }

        loginViewModel.rx_update.subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull String s) {
                dialog.dismiss();
                myEdit.putString("UserName", s);
                myEdit.commit();
                Intent intent1 = new Intent(TAccountActivity.this, MainActivity.class);
                intent1.putExtra("UserName", s);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void createAccount(View view) {
        Intent intent = new Intent(getBaseContext(), TCreateAccountActivity.class);
        // intent.putExtras(getIntent().getExtras());
        startActivityForResult(intent, REQ_REGISTER);
    }

    public void login(View view) {
        final String userId = ((EditText) findViewById(R.id.user)).getText().toString();
        final String passWd = ((EditText) findViewById(R.id.password)).getText().toString();

        final String accountType = getIntent().getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);

        new AsyncTask<Void, Void, Intent>() {
            @Override
            protected Intent doInBackground(Void... params) {
                Bundle data = new Bundle();
                final DateFormat df = new SimpleDateFormat("yyyyMMdd-HHmmss");
                Account loginAccount = AccountUtils.checkPasswordIsCorrect(TAccountActivity.this, userId, passWd);
                if (loginAccount == null) {

                    return null;
                }
                String authToken = userId + "-" + df.format(new Date());
                // String authToken = ZoftinoAccountRegLoginHelper.authenticate(userId, passWd);
                //  String tokenType = ZoftinoAccountRegLoginHelper.getTokenType(userId);

                data.putString(AccountManager.KEY_ACCOUNT_NAME, userId);
                data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                data.putString(TAccountAuthenticator.TOKEN_TYPE, TAccountAuthenticator.TOKEN_TYPE);
                data.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                data.putString(TAccountAuthenticator.PASSWORD, passWd);

                final Intent result = new Intent();
                result.putExtras(data);

                return result;
            }

            @Override
            protected void onPostExecute(Intent intent) {
                setLoginResult(intent);
            }
        }.execute();
    }

    private void setLoginResult(Intent intent) {
        if (intent == null) {
            Toast.makeText(TAccountActivity.this, "Invalid username/password. Please create a user.", Toast.LENGTH_SHORT).show();
            return;
        }
        userId = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        String passWd = intent.getStringExtra(TAccountAuthenticator.PASSWORD);

        final Account account = new Account(userId, AccountUtils.ACCOUNT_TYPE);

        if (intent.getExtras().getBoolean(TAccountAuthenticator.ADD_ACCOUNT, false) == true) {
            String authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            String tokenType = intent.getStringExtra(TAccountAuthenticator.TOKEN_TYPE);

            accountManager.addAccountExplicitly(account, passWd, null);
            accountManager.setAuthToken(account, tokenType, authtoken);
        } else {


            myEdit.putString("UserName", account.name);
            myEdit.putString("password", account.type);
            myEdit.commit();
            accountManager.setPassword(account, passWd);
            Intent intent1 = new Intent(TAccountActivity.this, MainActivity.class);
            intent1.putExtra("UserName", userId);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent1);
        }

        //setAccountAuthenticatorResult(intent.getExtras());
        // setResult(RESULT_OK, intent);
//
        // finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQ_REGISTER) {
            setLoginResult(data);
        } else {
            // viewModel.login(editTextUserName.getText().toString(), etPassword.getText().toString());
            Intent intent = new Intent(TAccountActivity.this, MainActivity.class);
            intent.putExtra("UserName", userId);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}