package com.example.kt_telematicinterview.useraccounts;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.kt_telematicinterview.R;

public class TCreateAccountActivity extends AppCompatActivity {

    private String accountType;
    AccountManager mAccountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAccountManager = AccountManager.get(this);
        accountType = AccountUtils.ACCOUNT_TYPE;

    }

    public void createAccount(View view) {
        final String userId = ((EditText) findViewById(R.id.user)).getText().toString();
        final String passWd = ((EditText) findViewById(R.id.password)).getText().toString();
        final String name = ((EditText) findViewById(R.id.name)).getText().toString();

        //  if(!ZoftinoAccountRegLoginHelper.validateAccountInfo(name,userId, passWd)){
        //      ((TextView)findViewById(R.id.error)).setText("Please Enter Valid Information");
        //  }

        // String authToken = ZoftinoAccountRegLoginHelper.createAccount(name,userId, passWd);
        //Account account = new Account(userId, accountType);
        String authToken = name + userId + passWd;
       // mAccountManager.addAccountExplicitly(account, passWd, null);
       // mAccountManager.setAuthToken(account, accountType, authToken);
        // String authTokenType = ZoftinoAccountRegLoginHelper.getTokenType(userId);
        String authTokenType = TAccountAuthenticator.TOKEN_TYPE;
        if (authToken.isEmpty()) {
            ((TextView) findViewById(R.id.error)).setText("Account couldn't be registered, please try again.");
        }

        Bundle data = new Bundle();
        data.putBoolean(TAccountAuthenticator.ADD_ACCOUNT, true);
        data.putString(AccountManager.KEY_ACCOUNT_NAME, userId);
        data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
        data.putString(AccountManager.KEY_AUTHTOKEN, authToken);
        data.putString(TAccountAuthenticator.PASSWORD, passWd);
        data.putString(TAccountAuthenticator.TOKEN_TYPE, authTokenType);

        final Intent result = new Intent();
        result.putExtras(data);

        setResult(RESULT_OK, result);
       finish();
    }
}