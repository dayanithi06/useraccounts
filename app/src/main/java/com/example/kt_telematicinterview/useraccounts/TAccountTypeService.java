package com.example.kt_telematicinterview.useraccounts;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class TAccountTypeService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        TAccountAuthenticator authenticator = new TAccountAuthenticator(this);
        return authenticator.getIBinder();
    }
}