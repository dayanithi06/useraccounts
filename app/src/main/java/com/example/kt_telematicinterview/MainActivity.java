package com.example.kt_telematicinterview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.dagger.AppComponent;
import com.example.kt_telematicinterview.database.model.DetailModel;
import com.example.kt_telematicinterview.ui.adapter.MyListAdapter;
import com.example.kt_telematicinterview.ui.dialog.AddDetailsDialog;
import com.example.kt_telematicinterview.ui.dialog.SwitchAccountDialog;
import com.example.kt_telematicinterview.viewmodel.MainViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    ImageView imagebg;
    RecyclerView recyclerView;
    String UserNameFromIntent;
    MainViewModel model;
    Button button_adddetail;
    TextView textviewNameintial;
    AddDetailsDialog detailsDialog;
    SwitchAccountDialog dialog;
    MyListAdapter adapter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor myEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new MainViewModel();
        ((MyApplication) getApplication()).getAppComponent().inject(model);
        setContentView(R.layout.activity_main);
        UserNameFromIntent = getIntent().getStringExtra("UserName");
        sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE);
        myEdit = sharedPreferences.edit();
        intialize();
        setAdapter();
        setListner();
        registerobs();
        textviewNameintial.setText(UserNameFromIntent.substring(0, 1));
        model.rx_user_update.subscribe(new Observer<DetailModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(DetailModel detailModel) {
                model.dataManger.addDetails(detailModel);
                setAdapter();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        });

        model.rx_update.subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String s) {
                UserNameFromIntent = s;
                setAdapter();
                textviewNameintial.setText(UserNameFromIntent.substring(0, 1));
                dialog.dismiss();
                myEdit.putString("UserName", UserNameFromIntent);
                myEdit.commit();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void setListner() {
        imagebg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new SwitchAccountDialog();
                dialog.setViewModel(model);
                Bundle bundle = new Bundle();
                bundle.putString("username", UserNameFromIntent);
                dialog.setArguments(bundle);
                dialog.show(getSupportFragmentManager(), "acc");
            }
        });

        button_adddetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailsDialog = new AddDetailsDialog();
                detailsDialog.setViewModel(model);

                Bundle bundle = new Bundle();
                bundle.putString("username", UserNameFromIntent);
                detailsDialog.setArguments(bundle);
                detailsDialog.show(getSupportFragmentManager(), "aa");
            }
        });
    }

    private void setAdapter() {
        ArrayList<DetailModel> detailModelArrayList = model.dataManger.retriveDetailsBasedOnUserName(UserNameFromIntent);
        if (detailModelArrayList == null || detailModelArrayList.isEmpty()) {

            if (adapter != null) {
                adapter.clearItems();
                adapter.notifyDataSetChanged();
                recyclerView.invalidate();
            }
            return;
        }
        adapter = new MyListAdapter(detailModelArrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        recyclerView.invalidate();
    }

    private void intialize() {


        recyclerView = findViewById(R.id.recyclerView_details);
        imagebg = findViewById(R.id.imagebg);
        button_adddetail = findViewById(R.id.button_adddetail);
        textviewNameintial = findViewById(R.id.textviewNameintial);
    }

    public void registerobs() {


    }
}