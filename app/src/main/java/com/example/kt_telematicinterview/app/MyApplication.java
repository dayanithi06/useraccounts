package com.example.kt_telematicinterview.app;

import android.app.Application;
import android.content.Context;

import com.example.kt_telematicinterview.dagger.AppComponent;
import com.example.kt_telematicinterview.dagger.AppModule;
import com.example.kt_telematicinterview.dagger.DaggerAppComponent;
import com.example.kt_telematicinterview.data.DatabaseRealm;

import javax.inject.Inject;

public class MyApplication extends Application {

  public  AppComponent appComponent = null;
  public  Context context = null;

    @Inject
    DatabaseRealm realm;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        appComponent.inject(this);

        initRealm();
        getContext();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
    void getContext() {
        context = getApplicationContext();
    }


    void initRealm() {
        realm.setup();

    }

}