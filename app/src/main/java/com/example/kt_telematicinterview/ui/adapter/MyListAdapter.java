package com.example.kt_telematicinterview.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.kt_telematicinterview.R;
import com.example.kt_telematicinterview.database.model.DetailModel;
import com.example.kt_telematicinterview.database.model.UserDataModel;

import java.util.ArrayList;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder>{
    private ArrayList<DetailModel> listdata;
  
   // RecyclerView recyclerView;  
    public MyListAdapter(ArrayList<DetailModel> listdata) {
        this.listdata = listdata;  
    }  
    @Override  
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_detail_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);  
        return viewHolder;  
    }  
  
    @Override  
    public void onBindViewHolder(ViewHolder holder, int position) {  
        final DetailModel myListData = listdata.get(position);
        holder.textviewDescription.setText(myListData.getName().concat("\n").concat(myListData.getUsername()));

    }  
  
  
    @Override  
    public int getItemCount() {  
        return listdata.size();
    }  

    public void clearItems(){
        int size = listdata.size();
        listdata.clear();
        this.notifyDataSetChanged();
        this.notifyItemRangeChanged(0, size);


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {  
        public TextView textviewDescription;
        public ViewHolder(View itemView) {
            super(itemView);  
            this.textviewDescription = (TextView) itemView.findViewById(R.id.textviewDescription);
        }
    }  
}  