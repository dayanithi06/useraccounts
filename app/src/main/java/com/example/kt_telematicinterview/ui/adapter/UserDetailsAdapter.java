package com.example.kt_telematicinterview.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.kt_telematicinterview.R;
import com.example.kt_telematicinterview.database.model.DetailModel;
import com.example.kt_telematicinterview.database.model.UserDataModel;
import com.example.kt_telematicinterview.viewmodel.MainViewModel;

import java.util.ArrayList;

public class UserDetailsAdapter extends RecyclerView.Adapter<UserDetailsAdapter.ViewHolder> {
    private final Context context;
    private ArrayList<UserDataModel> listdata;
    MainViewModel viewModel;

    // RecyclerView recyclerView;
    public UserDetailsAdapter(ArrayList<UserDataModel> listdata, Context context) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_list_detailsuser, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final UserDataModel myListData = listdata.get(position);
        holder.textviewDescription.setText(myListData.getUsername());
        holder.textviewDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewModel.rx_update.onNext(myListData.getUsername());

            }
        });
    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public void setViewModel(MainViewModel model) {
        viewModel = model;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textviewDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textviewDescription = (TextView) itemView.findViewById(R.id.textviewDescription);
        }
    }
}