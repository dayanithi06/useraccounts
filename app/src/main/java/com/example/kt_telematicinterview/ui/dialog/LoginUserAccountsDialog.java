package com.example.kt_telematicinterview.ui.dialog;



import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kt_telematicinterview.R;
import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.database.model.DetailModel;
import com.example.kt_telematicinterview.database.model.UserDataModel;
import com.example.kt_telematicinterview.ui.adapter.MyListAdapter;
import com.example.kt_telematicinterview.ui.adapter.UserDetailsAdapter;
import com.example.kt_telematicinterview.useraccounts.AccountUtils;
import com.example.kt_telematicinterview.viewmodel.LoginViewModel;
import com.example.kt_telematicinterview.viewmodel.MainViewModel;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subscribers.DisposableSubscriber;

public class LoginUserAccountsDialog extends DialogFragment {

    RecyclerView recycerview_accounts;
    LinearLayout linearlayout_addaccount;
    String usernameFromIntent;
    MainViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertview = inflater.inflate(R.layout.dialog_login_useraccounts, container, false);

        ((MyApplication) getActivity().getApplication()).getAppComponent().inject(model);

        usernameFromIntent = getArguments().getString("username");
        intialize(convertview);
        setAdapter();
        setListner();
    /*    model.rx_update.subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String s) {
                dismiss();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });*/
        return convertview;
    }

    private void setListner() {

        linearlayout_addaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Please use login page",Toast.LENGTH_SHORT).show();
            }
        });
        linearlayout_addaccount.setVisibility(View.GONE);
    }

    public void intialize(View convertView) {
        recycerview_accounts = convertView.findViewById(R.id.recycerview_accounts);
        linearlayout_addaccount = convertView.findViewById(R.id.linearlayout_addaccount);
    }

    public void setAdapter() {

        //  ArrayList<UserDataModel> detailModelArrayList = model.dataManger.retriveAllDetails();
        ArrayList<UserDataModel> detailModelArrayList = AccountUtils.getAllAccounts(getActivity());
        UserDetailsAdapter adapter = new UserDetailsAdapter(detailModelArrayList, getActivity());
        adapter.setViewModel(model);
        recycerview_accounts.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycerview_accounts.setAdapter(adapter);
    }

    public void setViewModel(MainViewModel model) {
        this.model = model;
    }
}
