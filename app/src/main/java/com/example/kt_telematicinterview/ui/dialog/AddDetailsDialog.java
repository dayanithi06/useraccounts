package com.example.kt_telematicinterview.ui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.kt_telematicinterview.R;
import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.database.model.DetailModel;
import com.example.kt_telematicinterview.viewmodel.MainViewModel;

public class AddDetailsDialog extends DialogFragment {

    EditText edittext_name,
    edittext_area,
            edittext_city,
    edittext_state;
           Button button_done;
    MainViewModel mainViewModel;
    String userNameFromIntent;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertview = inflater.inflate(R.layout.dialog_enter_details,container,false);

        ((MyApplication) getActivity().getApplication()).getAppComponent().inject(mainViewModel);
        userNameFromIntent= getArguments().getString("username");
        intialize(convertview);
        setListner();
        return convertview;
    }
    public void intialize(View convertview){
        edittext_name=convertview.findViewById(R.id.edittext_name);
                edittext_area=convertview.findViewById(R.id.edittext_area);
        edittext_city=convertview.findViewById(R.id.edittext_city);
                edittext_state=convertview.findViewById(R.id.edittext_state);
        button_done=convertview.findViewById(R.id.button_done);
    }
    public void setListner(){
        button_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             if(validate()){
                 DetailModel detailModel = new DetailModel();
                 detailModel.setUsername(userNameFromIntent);
                 detailModel.setName(edittext_name.getText().toString());
                 detailModel.setArea(edittext_area.getText().toString());
                 detailModel.setCity(edittext_city.getText().toString());
                 detailModel.setState(edittext_state.getText().toString());
                mainViewModel.rx_user_update.onNext(detailModel);
                dismiss();
             }
            }
        });
    }
    public boolean validate(){
        if(edittext_name.getText().toString().isEmpty()||edittext_area.getText().toString().isEmpty()||edittext_city.getText().toString().isEmpty()||edittext_city.getText().toString().isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    public void setViewModel(MainViewModel model) {
        this.mainViewModel=model;
    }
}
