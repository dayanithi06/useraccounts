package com.example.kt_telematicinterview.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.kt_telematicinterview.MainActivity;
import com.example.kt_telematicinterview.R;
import com.example.kt_telematicinterview.useraccounts.TAccountActivity;

public class SplashActivity extends Activity {

    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashActivity.this, TAccountActivity.class);
                startActivity(intent);
                finish();
            }
        },5000);

    }
}