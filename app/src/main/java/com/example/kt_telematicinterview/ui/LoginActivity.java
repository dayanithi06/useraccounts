package com.example.kt_telematicinterview.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.kt_telematicinterview.MainActivity;
import com.example.kt_telematicinterview.R;
import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.viewmodel.LoginViewModel;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {

    Button buttonLogin = null;
    EditText editTextUserName = null;
    TextInputEditText etPassword = null;
    LoginViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        viewModel = new LoginViewModel();
        ((MyApplication) getApplication()).getAppComponent().inject(viewModel);

        initalize();
        setListner();
    }

    private void setListner() {
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate()) {
                    viewModel.login(editTextUserName.getText().toString(), etPassword.getText().toString());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("UserName",editTextUserName.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, "Enter Username/password", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private Boolean isValidate() {
        if (editTextUserName.getText().toString().length() > 0 && etPassword.getText().toString().length() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private void initalize() {
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        editTextUserName = (EditText) findViewById(R.id.editTextUserName);
        etPassword = (TextInputEditText) findViewById(R.id.etPassword);

    }

}