package com.example.kt_telematicinterview.dagger;

import com.example.kt_telematicinterview.MainActivity;
import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.data.DatabaseRealm;
import com.example.kt_telematicinterview.data.DataManagerImpl;
import com.example.kt_telematicinterview.ui.LoginActivity;
import com.example.kt_telematicinterview.viewmodel.LoginViewModel;
import com.example.kt_telematicinterview.viewmodel.MainViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class,})
public interface AppComponent {

    public void inject(MyApplication application);

    public void inject(DatabaseRealm databaseRealm);

    public void inject(DataManagerImpl dataManager);

    public void inject(MainActivity mainActivity) ;

    public void inject(MainViewModel mainViewModel) ;

    public void inject(LoginActivity loginActivity);

    public void inject(LoginViewModel loginViewModel);
}

