package com.example.kt_telematicinterview.dagger;

import android.content.Context;

import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.data.DataManagerImpl;
import com.example.kt_telematicinterview.data.DataManger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.example.kt_telematicinterview.data.DatabaseRealm;
@Module
public
class AppModule {
    private MyApplication mApplication;
 
    public AppModule(MyApplication mApplication) {
        this.mApplication = mApplication;
    }
 
    @Provides
    @Singleton
    MyApplication provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    public DatabaseRealm provideDatabaseRealm() {
        return new DatabaseRealm(mApplication);
    }




    @Provides
    @Singleton
    public Context applicationContext() {
        return mApplication.getApplicationContext();
    }


    @Provides
    @Singleton
    public DataManger providesDataManager() {
        return new DataManagerImpl(mApplication);
    }

}