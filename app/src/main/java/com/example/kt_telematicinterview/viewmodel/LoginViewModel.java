package com.example.kt_telematicinterview.viewmodel;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.data.DataManagerImpl;
import com.example.kt_telematicinterview.data.DataManger;
import com.example.kt_telematicinterview.data.DatabaseRealm;
import com.example.kt_telematicinterview.ui.LoginActivity;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class LoginViewModel extends Activity {

   public BehaviorSubject<String> rx_update = BehaviorSubject.create();
    @Inject
    DataManger dataManger;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApplication) getApplication()).getAppComponent().inject(this);

    }


    public void login(String username, String password) {
        dataManger.addUsers(username);

    }
}
