package com.example.kt_telematicinterview.viewmodel;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.kt_telematicinterview.MainActivity;
import com.example.kt_telematicinterview.app.MyApplication;
import com.example.kt_telematicinterview.data.DataManger;
import com.example.kt_telematicinterview.database.model.DetailModel;

import javax.inject.Inject;

import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class MainViewModel extends MainActivity {

   public BehaviorSubject<String> rx_update = BehaviorSubject.create();
   public BehaviorSubject<DetailModel> rx_user_update = BehaviorSubject.create();
@Inject
   public DataManger dataManger;
   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      ((MyApplication) getApplication()).getAppComponent().inject(this);

   }
}
